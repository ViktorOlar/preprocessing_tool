#include "perspective_correction.h"

cv::Mat perspective_correction(cv::Mat in_img, std::vector<cv::Point2i> roi_corners, std::vector<cv::Point2i> dst_corners)
{
	cv::Size2i new_size;
	new_size.width = in_img.cols + 180 * 2;
	new_size.height = in_img.rows + 167 * 2;

	cv::Mat resized_original_image;
	resized_original_image = cv::Mat::zeros(new_size.height, new_size.width, in_img.type());
	in_img.copyTo(resized_original_image(cv::Rect2i((new_size.width - in_img.cols) / 2, (new_size.height - in_img.rows) / 2, in_img.cols, in_img.rows)));

	cv::Size2i warped_image_size = cv::Size2i(resized_original_image.cols, resized_original_image.rows);
	cv::Mat H = cv::findHomography(roi_corners, dst_corners); //get homography

	cv::Mat warped_image;
	cv::warpPerspective(resized_original_image, warped_image, H, warped_image_size, cv::INTER_LINEAR + cv::WARP_FILL_OUTLIERS); // do perspective transformation

	return warped_image;
}
#include "frame.h"

Frame::Frame()
{
}

Frame::~Frame()
{
}

Frame * Frame::init(FrameSource source_)
{
	switch (source_)
	{

	case FrameSource::HSCAM_FRAME: {
		return new HSCamFrame();
	}
	case FrameSource::RAILEYE_FRAME: {
		return new RailEyeFrame();
	}
	case FrameSource::TRAINPACK_FRAME: {
		return new TrainPackFrame();
	}

	}

	//return nullptr;
}


HSCamFrame::HSCamFrame()
{
}

HSCamFrame::~HSCamFrame()
{
	if (!this->mat.empty())
	{
		this->mat.release();
	}
}


cv::Mat HSCamFrame::getMat()
{
	return this->mat;
}

void HSCamFrame::setMat(cv::Mat& inMat)
{
	inMat.copyTo(this->mat);
}

RailEyeFrame::RailEyeFrame()
{
}

RailEyeFrame::~RailEyeFrame()
{
	//if (!this->mat.empty())
	//{
	//	this->mat.release();
	//}
}

cv::Mat RailEyeFrame::getMat()
{
	return this->mat;
}

void RailEyeFrame::setMat(cv::Mat& inMat)
{
	inMat.copyTo(this->mat);
	//this->mat = inMat.clone();
}

TrainPackFrame::TrainPackFrame()
{
}

TrainPackFrame::~TrainPackFrame()
{
}

cv::Mat TrainPackFrame::getMat()
{
	return this->mat;
}

void TrainPackFrame::setMat(cv::Mat& inMat)
{
	inMat.copyTo(this->mat);
}

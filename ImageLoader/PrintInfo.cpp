#include "PrintInfo.h"
#include "hscamimport.h"

#pragma once
void CheckError(IOErrorCodes err)
{
	switch (err)
	{
	case ERR_PATH_NOT_FOUND: throw std::exception("ERR_PATH_NOT_FOUND");
	case ERR_INVALID_STREAM_NO: throw std::exception("ERR_INVALID_STREAM_NO");
	case ERR_INVALID_RECORDING: throw std::exception("ERR_INVALID_RECORDING");
	case ERR_STREAM_NOT_FOUND: throw std::exception("ERR_STREAM_NOT_FOUND");
	case ERR_INVALID_STREAM_INFO: throw std::exception("ERR_INVALID_STREAM_INFO");
	case ERR_STREAM_NOT_OPEN: throw std::exception("ERR_STREAM_NOT_OPEN");
	case ERR_RECORDING_EXISTS: throw std::exception("ERR_RECORDING_EXISTS");
	case ERR_INVALID_STREAM: throw std::exception("ERR_INVALID_STREAM");
	case ERR_ADD_IMAGE_TO_STREAM: throw std::exception("ERR_ADD_IMAGE_TO_STREAM");

	}
}

void CheckError(int err)
{
	switch (err)
	{
	case hscamimport::ERR_FILE_NOT_FOUND: throw std::exception("ERR_FILE_NOT_FOUND");
	case hscamimport::ERR_VERSION_NOT_SUPPORTED: throw std::exception("ERR_VERSION_NOT_SUPPORTED");
	case hscamimport::ERR_UNKOWN_DATA_FORMAT: throw std::exception("ERR_UNKOWN_DATA_FORMAT");
	}
}

void PrintRecordingInfo(CRecording* pRec)
{
	std::cout << "RecordingName = " << pRec->GetRecordingName() << "/r/n";
	std::cout << "FilePath = " << pRec->GetFilePath() << "/r/n";
	std::cout << "CreationDate = " << pRec->GetCreationDate() << "/r/n";
	std::cout << "NumStreams = " << pRec->GetNumStreams() << "/r/n";
}

void PrintStreamInfo(CVideoStream* pStream)
{
	std::cout << "RecordingName = " << pStream->GetStreamName() << "/r/n";
	std::cout << "NumFrames = " << pStream->GetNumFrames() << "/r/n";
	std::cout << "AbsoluteStartTime = " << pStream->GetAbsoluteStartTime() << "/r/n";
	std::cout << "Duration (ms) = " << (pStream->GetDuration() / 1000000.0) << "/r/n";
	std::cout << "DataFormat = " << pStream->GetDataFormat() << "/r/n";
}

std::string ByteArrayToHex(unsigned char* pData, int iByteCount)
{
	std::string strRet;
	char hexstr[3];
	for (int i = 0; i < iByteCount; i++)
	{
		sprintf_s(hexstr, sizeof(hexstr), "%02x", pData[i]);
		hexstr[2] = 0;
		strRet += hexstr;
		strRet += " ";
	}

	return strRet;
}

void PrintFrameInfo(CVideoFrame* pFrame)
{
	std::cout << "Height = " << pFrame->GetHeight() << "/r/n";
	std::cout << "Width = " << pFrame->GetWidth() << "/r/n";
	//std::cout << "RecordingName = " << pFrame->GetData();
	std::cout << "PayloadSize = " << pFrame->GetPayloadSize() << "/r/n";
	if (pFrame->GetPayloadSize() > 10)
	{
		std::cout << "Data = " << ByteArrayToHex((unsigned char*)pFrame->GetData(), 10) << ".../r/n";
	}
	std::cout << "TimestampRelative = " << pFrame->GetTimestampRelative() << "/r/n";
	std::cout << "TimestampAbsolute = " << pFrame->GetTimestampAbsolute() << "/r/n";
	std::cout << "FrameNumber = " << pFrame->GetFrameNumber() << "/r/n";
}

__int64 GetRandFrameNo(CVideoStream* pStream)
{
	int iDurationInMs = (int)(pStream->GetDuration() / 1000000);
	_int64 iFrameAtTime = rand() % iDurationInMs;
	std::cout << "Get Frame at " << iFrameAtTime << " ms,";
	iFrameAtTime *= 1000000;
	__int64 iFrame = pStream->ConvertRelTimeToNumFrame(iFrameAtTime);
	std::cout << "FrameNo " << iFrame << ".../r/n";
	return iFrame;
}

__int64 GetAktTimeInNano()
{
	using namespace std::chrono;
	return duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count();
}
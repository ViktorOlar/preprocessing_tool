#pragma once
#include <iostream>
#include <Recording.h>
#include <VideoStream.h>
#include <VideoFrame.h>
#include <Defines.h>
#include <string>
#include <chrono>

void CheckError(IOErrorCodes err);//for RailEye
void CheckError(int err);//for HSCAM
void PrintRecordingInfo(CRecording* pRec);
void PrintStreamInfo(CVideoStream* pStream);
std::string ByteArrayToHex(unsigned char* pData, int iByteCount);
void PrintFrameInfo(CVideoFrame* pFrame);
__int64 GetRandFrameNo(CVideoStream* pStream);
__int64 GetAktTimeInNano();
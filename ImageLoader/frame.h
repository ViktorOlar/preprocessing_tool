#pragma once

#include <opencv2/opencv.hpp>
class Frame
{
public:
	enum class FrameSource
	{
		HSCAM_FRAME,
		RAILEYE_FRAME,
		TRAINPACK_FRAME
	};

	Frame();
	virtual ~Frame() = 0;

	static Frame* init(FrameSource source_);
	virtual cv::Mat getMat() = 0;
	virtual void setMat(cv::Mat& inMat) = 0;
	size_t payloadSize;
	__int64 timestampRelative;
	__int64 timestampAbsolute;
	unsigned __int64 FrameNumber;

	std::string name;
private:
	cv::Mat mat;

};

class HSCamFrame : public Frame
{
public:
	HSCamFrame();
	~HSCamFrame();
	//void ~Frame();

	cv::Mat getMat();
	void setMat(cv::Mat& inMat);
	size_t payloadSize;
	__int64 timestampRelative;
	__int64 timestampAbsolute;
	unsigned __int64 FrameNumber;

private:
	cv::Mat mat;
};

class RailEyeFrame : public Frame
{
public:
	RailEyeFrame();
	~RailEyeFrame();

	cv::Mat getMat();
	void setMat(cv::Mat& inMat);
	size_t payloadSize;
	__int64 timestampRelative;
	__int64 timestampAbsolute;
	unsigned __int64 FrameNumber;

private:
	cv::Mat mat;
};

class TrainPackFrame : public Frame
{
public:
	TrainPackFrame();
	~TrainPackFrame();
	//void ~Frame();

	cv::Mat getMat();
	void setMat(cv::Mat& inMat);
	size_t payloadSize;
	__int64 timestampRelative;
	__int64 timestampAbsolute;
	unsigned __int64 FrameNumber;

	std::string name;
private:
	cv::Mat mat;
};
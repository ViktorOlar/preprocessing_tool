#pragma once

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <mutex>
#include <opencv2/opencv.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem.hpp>

#include "frame.h"
#include "PrintInfo.h"
#include "hscamimport.h"
//include RailEye headers
#include <VideoStream.h>
#include <Recording.h>
#include <VideoFrame.h>
#include "Defines.h"

#include "../libwebp/libwebp_iofunctions.h"
extern libwebp libwebpobj;

#define HSCAM_ENV 0
#define RAILEYE_ENV 1
#define TRAINPACK_ENV 2



class ImageLoader
{
public:
	enum class ImageLoaderSource
	{
		HSCAM,
		RAILEYE,
		TRAINPACK
	};

	ImageLoader();
	virtual ~ImageLoader() = 0;
	static ImageLoader* init(ImageLoaderSource source_);
	virtual void printINFO() = 0;


	// pure virtual functions to recording
	virtual int load(std::string path_, int* errCode, std::map<int, std::string>& streamsIndexes) = 0;
	virtual std::string getRecordingName() = 0;
	virtual std::time_t getCreationDate() = 0;
	virtual unsigned int getNumStreams() = 0;
	virtual unsigned int getNumEvents() = 0;

	// pure virtual funtions to handle streams
	virtual std::string getStreamName(int streamID_) = 0;
	virtual Frame* getVideoFrameByFrameNumberFromStream(int streamID_, uint64_t frameNumber_, bool loadMetaData) = 0;
	virtual Frame* getVideoFrameByRelTimeFromStream(int streamID_, uint64_t timestamp, bool loadMetaData) = 0;
	virtual int getNumFramesInStream(int streamID_) = 0;
	virtual double getFpsRateFromStream(int streamID_) = 0;

	virtual __int64 getAbsoluteStartTimeOfStream(int streamID_) = 0;
	virtual __int64 getRelStartTimeOfStream(int streamID_) = 0;
	virtual __int64 getDurationOfStream(int streamID_) = 0;
	virtual __int64 convertRelTimeToNumFrame(int streamID_, __int64 t) = 0;
	virtual __int64 convertNumFrameToRelTime(int streamID_, unsigned __int64 nFrame) = 0;

	//virtual CamVideoDataFormat dataFormat() = 0;
	virtual hscamimport::HSEvent* event(int eventNumber_) = 0;

	std::map<int, int> streamErrors;
protected:
	static ImageLoaderSource SOURCE;
};

class HSCamLoader : public ImageLoader
{
public:
	HSCamLoader();
	~HSCamLoader();

	int load(std::string path, int* errCode, std::map<int, std::string>& streamsIndexes);
	std::string getStreamName(int streamID_);
	Frame* getVideoFrameByFrameNumberFromStream(int streamID_, uint64_t frameNumber_, bool loadMetaData);
	Frame* getVideoFrameByRelTimeFromStream(int streamID_, uint64_t timestamp, bool loadMetaData);
	unsigned int getNumStreams(void);
	int getNumFramesInStream(int streamID_);
	double getFpsRateFromStream(int streamID_);

	std::string getRecordingName();
	std::time_t getCreationDate() ;
	unsigned int getNumEvents();

	int64_t getAbsoluteStartTimeOfStream(int streamID_);
	int64_t getRelStartTimeOfStream(int streamID_);
	int64_t getDurationOfStream(int streamID_);
	int64_t convertRelTimeToNumFrame(int streamID_, __int64 t);
	int64_t convertNumFrameToRelTime(int streamID_, unsigned __int64 nFrame);

	//CamVideoDataFormat dataFormat();
	hscamimport::HSEvent* event(int eventNumber_);

	void printINFO();

	std::map<int, int> streamErrors;
	hscamimport::HSRecording* recording = NULL;
protected:
	std::map<int, hscamimport::HSVideoStream*> streams;
};


class RailEyeLoader : public ImageLoader
{
public:
	RailEyeLoader();
	~RailEyeLoader();

	int load(std::string path, int* errCode, std::map<int, std::string>& streamsIndexes);
	std::string getStreamName(int streamID_);
	Frame* getVideoFrameByFrameNumberFromStream(int streamID_, uint64_t frameNumber_, bool loadMetaData);
	Frame* getVideoFrameByRelTimeFromStream(int streamID_, uint64_t timestamp, bool loadMetaData);
	unsigned int getNumStreams(void);
	int getNumFramesInStream(int streamID_);
	double getFpsRateFromStream(int streamID_);
	
	std::string getRecordingName();
	std::time_t getCreationDate();
	unsigned int getNumEvents();

	__int64 getAbsoluteStartTimeOfStream(int streamID_);
	__int64 getRelStartTimeOfStream(int streamID_);
	__int64 getDurationOfStream(int streamID_);
	__int64 convertRelTimeToNumFrame(int streamID_, __int64 t);
	__int64 convertNumFrameToRelTime(int streamID_, unsigned __int64 nFrame);

	//CamVideoDataFormat dataFormat();
	hscamimport::HSEvent* event(int eventNumber_);

	void printINFO();

	std::map<int, int> streamErrors;
	CRecording recording;
protected:
	std::map<int, CVideoStream*> streams;
};


class TrainPackLoader : public ImageLoader
{
public:
	TrainPackLoader();
	~TrainPackLoader();

	int load(std::string path, int* errCode, std::map<int, std::string>& streamsIndexes);
	std::string getStreamName(int streamID_);//returns names of folders with images
	Frame* getVideoFrameByFrameNumberFromStream(int streamID_, uint64_t frameNumber_, bool loadMetaData);
	Frame* getVideoFrameByRelTimeFromStream(int streamID_, uint64_t timestamp, bool loadMetaData);
	unsigned int getNumStreams(void);
	int getNumFramesInStream(int streamID_);
	double getFpsRateFromStream(int streamID_);

	std::string getRecordingName();
	std::time_t getCreationDate();
	unsigned int getNumEvents();

	__int64 getAbsoluteStartTimeOfStream(int streamID_);
	__int64 getRelStartTimeOfStream(int streamID_);
	__int64 getDurationOfStream(int streamID_);
	__int64 convertRelTimeToNumFrame(int streamID_, __int64 t);
	__int64 convertNumFrameToRelTime(int streamID_, unsigned __int64 nFrame);

	//CamVideoDataFormat dataFormat();
	hscamimport::HSEvent* event(int eventNumber_);
	static void writeXML(boost::filesystem::path& filePath, ImageLoader* ImagesLoader, std::map<int, std::string>& streamsIndexes);

	void printINFO();
	std::map<int, int> streamErrors;
protected:
	cv::String parentDir;
	std::map<int, boost::filesystem::path> streams;
	int numStreams;

	std::map<int, std::vector<boost::filesystem::path>> streamFrameNames;
	//boost::property_tree::ptree framesInfoXMLtree;
	std::map<int, std::string> streamsIndexes;
	int64_t getAbsoluteTimestampFromString(std::string frameName);
	int64_t getRelativeTimestamp(std::string frameName, int streamID_);
};

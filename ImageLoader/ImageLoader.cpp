#include "ImageLoader.h"
#include "frame.h"

//std::mutex imloader_frame_mutex;
ImageLoader::ImageLoader()
{

}

ImageLoader::~ImageLoader()
{
}

ImageLoader* ImageLoader::init(ImageLoaderSource source_)
{

	switch (source_)
	{

	case ImageLoaderSource::HSCAM: {
		return new HSCamLoader();
	}
	case ImageLoaderSource::RAILEYE: {
		return new RailEyeLoader();
	}
	case ImageLoaderSource::TRAINPACK: {
		return new TrainPackLoader();
	}

	}
}

HSCamLoader::HSCamLoader()
{
}

HSCamLoader::~HSCamLoader()
{
	for (auto& stream : streams) {
		delete stream.second;
	}
	delete recording;
}

int HSCamLoader::load(std::string path, int* errCode, std::map<int, std::string>& streamsIndexes)
{
	int errorCode = 0;
	recording = hscamimport::loadRecording(path.c_str(), errorCode);
	*errCode = errorCode;
	//CheckError(errorCode);

	for (int i = 0; i < recording->getNumStreams(); ++i) {
		streams.insert(std::make_pair(i, recording->openStream(i, errorCode)));
		streamErrors.insert(std::make_pair(i, static_cast<int>(errorCode)));
		//CheckError(errorCode);
	}

	return 0;
}

std::string HSCamLoader::getStreamName(int streamID_)
{
	//int streamNR = static_cast<int>(streamID_);
	/*if (streamNR == 0)
	{
		streamNR = 1;
	}
	else if (streamNR == 1)
	{
		streamNR = 0;
	}*/

	return this->streams[streamID_]->getStreamName();
}

Frame* HSCamLoader::getVideoFrameByFrameNumberFromStream(int streamID_, uint64_t frameNumber_, bool loadMetaData)
{

	/*int streamNR = static_cast<int>(streamID_);
	if (streamNR == 0)
	{
		streamNR = 1;
	}
	else if (streamNR == 1)
	{
		streamNR = 0;
	}*/

	Frame* ret_frame = NULL;
	ret_frame = Frame::init(Frame::FrameSource::HSCAM_FRAME);
	int strLen = streams[streamID_]->getNumFrames();
	hscamimport::HSVideoFrame* HSframe = NULL;

	HSframe = streams[streamID_]->getVideoFrameByFrameNumber(frameNumber_);
	cv::Mat mat;
	if (HSframe == NULL)
	{
		HSframe = streams[streamID_]->getVideoFrameByFrameNumber(0);
		int h = HSframe->height();
		int w = HSframe->width();
		cv::Mat(h, w, CV_8UC1, cv::Scalar(0, 0, 0)).copyTo(mat);
		delete HSframe;
		ret_frame->setMat(mat);
		ret_frame->FrameNumber = frameNumber_;
		return ret_frame;
	}

	const int sizes[2] = { HSframe->height(), HSframe->width() };
	cv::Mat(2, sizes, CV_8UC1, HSframe->data(), 0).copyTo(mat);

	ret_frame->setMat(mat);
	ret_frame->FrameNumber = HSframe->getFrameNumber();
	if (loadMetaData)
	{
		ret_frame->payloadSize = HSframe->payloadSize();
		ret_frame->timestampAbsolute = HSframe->timestampAbsolute();
		ret_frame->timestampRelative = HSframe->timestampRelative();
	}

	delete HSframe;
	return ret_frame;
}

Frame* HSCamLoader::getVideoFrameByRelTimeFromStream(int streamID_, uint64_t timestamp, bool loadMetaData)
{


	Frame* ret_frame = NULL;
	cv::Mat mat;

	uint64_t iDuration = this->getDurationOfStream(streamID_);
	if (timestamp > iDuration)
	{
		throw std::runtime_error("Input timestamp exceeds the stream duration !");
	}

	ret_frame = Frame::init(Frame::FrameSource::HSCAM_FRAME);

	hscamimport::HSVideoFrame* HSframe = NULL;
	HSframe = streams[streamID_]->getVideoFrameByRelTime(timestamp);
	if (HSframe == NULL)
	{
		HSframe = streams[streamID_]->getVideoFrameByFrameNumber(0);
		int h = HSframe->height();
		int w = HSframe->width();
		cv::Mat(h, w, CV_8UC1, cv::Scalar(0, 0, 0)).copyTo(mat);
		delete HSframe;
		ret_frame->setMat(mat);
		ret_frame->FrameNumber = 0;
		return ret_frame;
	}
	const int sizes[2] = { HSframe->height(), HSframe->width() };
	cv::Mat(2, sizes, CV_8UC1, HSframe->data(), 0).copyTo(mat);

	ret_frame->setMat(mat);
	ret_frame->FrameNumber = HSframe->getFrameNumber();
	if (loadMetaData)
	{
		ret_frame->payloadSize = HSframe->payloadSize();
		ret_frame->timestampAbsolute = HSframe->timestampAbsolute();
		ret_frame->timestampRelative = HSframe->timestampRelative();
	}
	delete HSframe;
	return ret_frame;
}

unsigned int HSCamLoader::getNumStreams(void)
{
	return recording->getNumStreams();
}

int HSCamLoader::getNumFramesInStream(int streamID_)
{
	/*int streamNR = static_cast<int>(streamID_);
	if (streamNR == 0)
	{
		streamNR = 1;
	}
	else if (streamNR == 1)
	{
		streamNR = 0;
	}*/
	return streams[streamID_]->getNumFrames();
}

double HSCamLoader::getFpsRateFromStream(int streamID_)
{
	return streams[streamID_]->getNumFrames() * 1000000000 / streams[streamID_]->getDuration();
}

std::string HSCamLoader::getRecordingName() {
	return this->recording->getRecordingName();
}

std::time_t HSCamLoader::getCreationDate() {
	return this->recording->getCreationDate();
}

unsigned int HSCamLoader::getNumEvents() {
	return this->recording->getNumEvents();
}

int64_t HSCamLoader::getAbsoluteStartTimeOfStream(int streamID_)
{
	/*int streamNR = static_cast<int>(streamID_);
	if (streamNR == 0)
	{
		streamNR = 1;
	}
	else if (streamNR == 1)
	{
		streamNR = 0;
	}*/

	int64_t t_ret;
	Frame* frame0 = NULL;
	frame0 = this->getVideoFrameByFrameNumberFromStream(streamID_, 0, true);
	t_ret = frame0->timestampAbsolute;
	delete frame0;
	return t_ret;

}

int64_t HSCamLoader::getRelStartTimeOfStream(int streamID_)
{
	/*int streamNR = static_cast<int>(streamID_);
	if (streamNR == 0)
	{
		streamNR = 1;
	}
	else if (streamNR == 1)
	{
		streamNR = 0;
	}*/
	return this->streams[streamID_]->getStartTime();
}

int64_t HSCamLoader::getDurationOfStream(int streamID_)
{
	return this->streams[streamID_]->getDuration();
}

int64_t HSCamLoader::convertRelTimeToNumFrame(int streamID_, __int64 t)
{
	/*int streamNR = static_cast<int>(streamID_);
	if (streamNR == 0)
	{
		streamNR = 1;
	}
	else if (streamNR == 1)
	{
		streamNR = 0;
	}*/
	return this->streams[streamID_]->convertRelTimeToNumFrame(t);
}

int64_t HSCamLoader::convertNumFrameToRelTime(int streamID_, unsigned __int64 nFrame)
{
	return this->streams[streamID_]->convertNumFrameToRelTime(nFrame);
}

hscamimport::HSEvent* HSCamLoader::event(int eventNumber_)
{
	return this->recording->event(eventNumber_);
}

void HSCamLoader::printINFO()
{
	std::cout << "HSCAM" << std::endl;
	std::cout << "Streams initialized: " << streams.size() << std::endl;
}

RailEyeLoader::RailEyeLoader()
{
}

RailEyeLoader::~RailEyeLoader()
{
	for (auto& stream : streams) {
		if (stream.second->IsOpen())
		{
			stream.second->CloseStream();
		}
	}
	streams.clear();
}

int RailEyeLoader::load(std::string path, int* errCode, std::map<int, std::string>& streamsIndexes)
{
	std::string npath = path;
	std::replace(npath.begin(), npath.end(), '/', '\\');
	IOErrorCodes errorCode = this->recording.OpenRecording(npath.c_str());
	*errCode = static_cast<int>(errorCode);
	if (*errCode != 0)
	{
		std::cout << "Could not open recording: " << npath << " Error code: " << *errCode << std::endl;
		std::exit(EXIT_FAILURE);
	}
	//CheckError(errorCode);

	for (int i = 0; i < recording.GetNumStreams(); ++i) {
		streams.insert(std::make_pair(i, recording.GetStream(i, errorCode)));

		CheckError(errorCode);
	}

	int j = 0;
	for (auto& stream : streams)
	{
		errorCode = stream.second->OpenStream();
		streamErrors.insert(std::make_pair(j, static_cast<int>(errorCode)));
		++j;
		//CheckError(errorCode);
	}

	return 0;
}

std::string RailEyeLoader::getStreamName(int streamID_)
{
	return this->streams[streamID_]->GetStreamName();
}


Frame* RailEyeLoader::getVideoFrameByFrameNumberFromStream(int streamID_, uint64_t frameNumber_, bool loadMetaData)
{
	Frame* ret_frame = NULL;
	ret_frame = Frame::init(Frame::FrameSource::RAILEYE_FRAME);
	//int streamNR = static_cast<int>(streamID_);
	bool flag = this->streams[streamID_]->IsOpen();
	int frNumber = this->streams[streamID_]->GetNumFrames();
	cv::Mat mat;
	CVideoFrame* RailEyeFrame = NULL;
	RailEyeFrame = this->streams[streamID_]->GetVideoFrameByFrameNumber(frameNumber_);
	if (RailEyeFrame == NULL)
	{
		RailEyeFrame = streams[streamID_]->GetVideoFrameByFrameNumber(0);
		int h = RailEyeFrame->GetHeight();
		int w = RailEyeFrame->GetWidth();
		cv::Mat(h, w, CV_8UC1, cv::Scalar(0, 0, 0)).copyTo(mat);
		delete RailEyeFrame;
		ret_frame->setMat(mat);
		ret_frame->FrameNumber = 0;
		return ret_frame;
	}
	const int sizes[2] = { RailEyeFrame->GetHeight(), RailEyeFrame->GetWidth() };
	cv::Mat(2, sizes, CV_8UC1, RailEyeFrame->GetData(), 0).copyTo(mat);

	ret_frame->setMat(mat);
	ret_frame->FrameNumber = frameNumber_;
	if (loadMetaData)
	{
		ret_frame->payloadSize = RailEyeFrame->GetPayloadSize();
		ret_frame->timestampAbsolute = RailEyeFrame->GetTimestampAbsolute();
		ret_frame->timestampRelative = this->streams[streamID_]->ConvertNumFrameToRelTime(frameNumber_);
	}
	delete RailEyeFrame;
	return ret_frame;
}

Frame* RailEyeLoader::getVideoFrameByRelTimeFromStream(int streamID_, uint64_t timestamp, bool loadMetaData)
{
	Frame* ret_frame = NULL;
	ret_frame = Frame::init(Frame::FrameSource::RAILEYE_FRAME);
	//int streamNR = static_cast<int>(streamID_);
	cv::Mat mat;
	CVideoFrame* RailEyeFrame = NULL;
	uint64_t iDuration = this->getDurationOfStream(streamID_);
	if (timestamp > iDuration)
	{
			throw std::runtime_error("Input timestamp exceeds the stream duration !");
	}
	else
	{
		RailEyeFrame = streams[streamID_]->GetVideoFrameByRelTime(timestamp);
		ret_frame->FrameNumber = streams[streamID_]->ConvertRelTimeToNumFrame(timestamp);
	}


	if (RailEyeFrame == NULL)
	{
		RailEyeFrame = streams[streamID_]->GetVideoFrameByFrameNumber(0);
		int h = RailEyeFrame->GetHeight();
		int w = RailEyeFrame->GetWidth();
		cv::Mat(h, w, CV_8UC1, cv::Scalar(0, 0, 0)).copyTo(mat);
		delete RailEyeFrame;
		ret_frame->setMat(mat);
		ret_frame->FrameNumber = 0;
		ret_frame->timestampAbsolute = timestamp;
		return ret_frame;
	}
	const int sizes[2] = { RailEyeFrame->GetHeight(), RailEyeFrame->GetWidth() };
	cv::Mat(2, sizes, CV_8UC1, RailEyeFrame->GetData(), 0).copyTo(mat);

	ret_frame->setMat(mat);
	//ref_frame->FrameNumber = RailEyeFrame->GetFrameNumber();

	if (loadMetaData)
	{
		ret_frame->payloadSize = RailEyeFrame->GetPayloadSize();
		ret_frame->timestampAbsolute = RailEyeFrame->GetTimestampAbsolute();
		//ref_frame->timestampRelative = RailEyeFrame->GetTimestampRelative();
		ret_frame->timestampRelative = this->streams[streamID_]->ConvertNumFrameToRelTime(ret_frame->FrameNumber);
	}
	delete RailEyeFrame;
	return ret_frame;
}

unsigned int RailEyeLoader::getNumStreams(void)
{
	return this->recording.GetNumStreams();
}

int RailEyeLoader::getNumFramesInStream(int streamID_)
{
	return streams[streamID_]->GetNumFrames();
}

double RailEyeLoader::getFpsRateFromStream(int streamID_)
{
	return streams[streamID_]->GetNumFrames() * 1000000000 / streams[streamID_]->GetDuration();
}

std::string RailEyeLoader::getRecordingName()
{
	return this->recording.GetRecordingName();
}

std::time_t RailEyeLoader::getCreationDate()
{
	return this->recording.GetCreationDate();
}

unsigned int  RailEyeLoader::getNumEvents()
{
	return 0;
}

__int64 RailEyeLoader::getAbsoluteStartTimeOfStream(int streamID_)
{
	return this->streams[streamID_]->GetAbsoluteStartTime();
}

__int64 RailEyeLoader::getRelStartTimeOfStream(int streamID_)
{
	return 0;
}

__int64 RailEyeLoader::getDurationOfStream(int streamID_)
{
	return this->streams[streamID_]->GetDuration();
}

__int64 RailEyeLoader::convertRelTimeToNumFrame(int streamID_, __int64 t)
{
	return this->streams[streamID_]->ConvertRelTimeToNumFrame(t);
}

__int64 RailEyeLoader::convertNumFrameToRelTime(int streamID_, unsigned __int64 nFrame)
{
	return this->streams[streamID_]->ConvertNumFrameToRelTime(nFrame);
}

//__int64 RailEyeLoader::getRelStartTimeOfStream(int streamID_)
//{
//	return this->streams[streamID_] -> ;
//}

hscamimport::HSEvent* RailEyeLoader::event(int eventNumber_)
{
	return nullptr;
}

void RailEyeLoader::printINFO()
{
	std::cout << "RAILEYE" << std::endl;
	std::cout << "RecordingName = " << recording.GetRecordingName() << "\r\n";
	std::cout << "FilePath = " << recording.GetFilePath() << "\r\n";
	std::cout << "CreationDate = " << recording.GetCreationDate() << "\r\n";
	std::cout << "NumStreams = " << recording.GetNumStreams() << "\r\n";
}

TrainPackLoader::TrainPackLoader()
{
}

TrainPackLoader::~TrainPackLoader()
{
}

int TrainPackLoader::load(std::string path, int* errCode, std::map<int, std::string>& streamsIndexes)
{
	int errorCode = 0;
	this->numStreams = 0;
	int counter = 0;

	this->parentDir = path;
	boost::filesystem::path bspath(path);
	this->streamsIndexes = streamsIndexes;

	if (boost::filesystem::exists(path))
	{
		//get list of folders (streams) in "path" folder
		for (auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(bspath), {})) {
			if (boost::filesystem::is_directory(entry))
			{
				streams.insert(std::make_pair(counter, entry));
				counter++;
			}
		}
		this->numStreams = counter;

		std::map<int, std::vector< boost::filesystem::path>> FrameNames;
		//get list of files in stream (folder)
		for (int i = 0; i < streams.size(); ++i)
		{
			//read filenames
			std::vector<boost::filesystem::path> names;
			for (auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(streams[i]), {}))
			{
				//if file with .xml extension found open it 
				if (boost::filesystem::is_regular_file(entry))//else get filenames (frames) contained in folder
				{
					names.push_back(entry);
				}
			}
			std::sort(names.begin(), names.end()
				, [](boost::filesystem::path const& a, boost::filesystem::path const& b) {
				return std::stoi(a.filename().string()) < std::stoi(b.filename().string());
			});
			FrameNames.insert(std::make_pair(i, names));

			//read XML file

		}

		this->streamFrameNames = FrameNames;
	}

	return 0;
}

std::string TrainPackLoader::getStreamName(int streamID_)
{
	return this->streams[streamID_].generic_string();
}

Frame* TrainPackLoader::getVideoFrameByFrameNumberFromStream(int streamID_, uint64_t frameNumber_, bool loadMetaData)
{
	Frame* ret_frame = NULL;
	ret_frame = Frame::init(Frame::FrameSource::TRAINPACK_FRAME);
	frameNumber_ >= this->getNumFramesInStream(streamID_) ? frameNumber_ = this->getNumFramesInStream(streamID_) - 1 : frameNumber_ = frameNumber_;
	frameNumber_ < 0 ? frameNumber_ = 0 : frameNumber_ = frameNumber_;
	cv::String streamName = this->streams[streamID_].generic_string();
	cv::String frameName = this->streamFrameNames[streamID_][frameNumber_].generic_string();
	cv::String filename = /*this->parentDir + streamName + */frameName;
	size_t lastindex = filename.find_last_of(".");
	std::string ext = filename.substr(lastindex, filename.length());
	cv::Mat mat;
	if (libwebpobj.enabled && ext == ".webp")
	{
		mat = libwebpobj.imread(filename);
	}
	else {
		mat = cv::imread(filename, cv::IMREAD_GRAYSCALE);
	}

	ret_frame->setMat(mat);
	ret_frame->FrameNumber = frameNumber_;
	ret_frame->name = this->streamFrameNames[streamID_][frameNumber_].stem().generic_string();

	if (loadMetaData)
	{
		ret_frame->timestampAbsolute = this->getAbsoluteTimestampFromString(frameName);
		ret_frame->timestampRelative = this->getRelativeTimestamp(frameName, streamID_);
	}

	return ret_frame;
}

Frame* TrainPackLoader::getVideoFrameByRelTimeFromStream(int streamID_, uint64_t t_in, bool loadMetaData)
{
	__int64 frameIndex = -1;
	uint64_t iDuration = this->getDurationOfStream(streamID_);
	if (t_in > iDuration)
	{
		throw std::runtime_error("Input timestamp exceeds the stream duration !");
	}
	else
	{
		frameIndex = this->convertRelTimeToNumFrame(streamID_, t_in);

	}
	Frame* ret_frame = NULL;
	frameIndex >= 0 ? frameIndex = frameIndex : frameIndex = 0;
	//frameIndex < this->getNumFramesInStream(streamID_) ? frameIndex = frameIndex : frameIndex= this->getNumFramesInStream(streamID_) - 1;
	ret_frame = this->getVideoFrameByFrameNumberFromStream(streamID_, frameIndex, loadMetaData);

	return ret_frame;
}

unsigned int TrainPackLoader::getNumStreams(void)
{
	return this->streams.size();
}

int TrainPackLoader::getNumFramesInStream(int streamID_)
{
	return static_cast<int>(this->streamFrameNames[streamID_].size());
}

double TrainPackLoader::getFpsRateFromStream(int streamID_)
{
	return (size_t)this->getNumFramesInStream(streamID_) * 1000000000 / this->getDurationOfStream(streamID_);
}

std::string TrainPackLoader::getRecordingName()
{
	return std::string();
}

std::time_t TrainPackLoader::getCreationDate()
{
	return std::time_t();
}

unsigned int TrainPackLoader::getNumEvents()
{
	return 0;
}

__int64 TrainPackLoader::getAbsoluteStartTimeOfStream(int streamID_)
{
	std::string streamName = this->streams[streamID_].generic_string();
	size_t undescorePos = streamName.find_last_of("_");
	std::string startTime = streamName.substr(undescorePos+1, streamName.length());
	return stoll(startTime);
}

__int64 TrainPackLoader::getRelStartTimeOfStream(int streamID_)
{
	std::string firstFrameName = this->streamFrameNames[streamID_][0].generic_string();

	int64_t relativeTimestamp = this->getRelativeTimestamp(firstFrameName, streamID_);
	return relativeTimestamp;
}

__int64 TrainPackLoader::getDurationOfStream(int streamID_)
{
	//return this->framesInfoXMLtree.get<__int64>("train." + this->streamsIndexes[streamID_] + ".Duration");
	int lenght = this->streamFrameNames[streamID_].size();
	std::string lastFrameName = this->streamFrameNames[streamID_][lenght - 1].generic_string();

	int64_t lastRelativeTimestamp = this->getRelativeTimestamp(lastFrameName, streamID_);

	return lastRelativeTimestamp;
}

__int64 TrainPackLoader::convertRelTimeToNumFrame(int streamID_, __int64 t_in)
{
	__int64 iIndex = -1;
	Frame* ref_frame = NULL;
	size_t iSize = this->getNumFramesInStream(streamID_);
	uint64_t iDuration = this->getDurationOfStream(streamID_);

	if (t_in > 0 && t_in <= iDuration)
	{
		iIndex = t_in / (iDuration / iSize);
		int iIndex_ascending = iIndex;//^
		uint64_t diff_ascending = 0;
		int iIndex_descending = iIndex;
		uint64_t diff_descending = 0;

		ref_frame = this->getVideoFrameByFrameNumberFromStream(streamID_, iIndex, true);
		uint64_t ref_timestamp = ref_frame->timestampRelative;

		uint64_t last_diff = std::abs(static_cast<long long>(ref_timestamp - t_in));
		diff_ascending = last_diff + 1;
		while (last_diff < diff_ascending && iIndex_ascending < iSize && (iIndex_ascending - iIndex) <= 2) {
			iIndex_ascending++;
			ref_frame = this->getVideoFrameByFrameNumberFromStream(streamID_, iIndex_ascending, true);
			ref_timestamp = ref_frame->timestampRelative;
			diff_ascending = std::abs(static_cast<long long>(ref_timestamp - t_in));
			delete ref_frame;
		}
		//iIndex_ascending--;
		ref_frame = this->getVideoFrameByFrameNumberFromStream(streamID_, iIndex, true);
		ref_timestamp = ref_frame->timestampRelative;

		//last_diff = std::abs(static_cast<long long>(ref_timestamp - t_in));
		diff_descending = last_diff + 1;
		while (last_diff < diff_descending && iIndex_descending >= 0 && iIndex_descending && (iIndex - iIndex_descending) <= 2) {
			iIndex_descending--;
			ref_frame = this->getVideoFrameByFrameNumberFromStream(streamID_, iIndex_descending, true);
			ref_timestamp = ref_frame->timestampRelative;
			diff_descending = std::abs(static_cast<long long>(ref_timestamp - t_in));
			delete ref_frame;
		}
		//iIndex_descending++;
		if (diff_ascending < diff_descending)
		{
			if (last_diff > diff_ascending)
			{
				iIndex = iIndex_ascending;
			}
		}
		else {
			if (last_diff > diff_descending)
			{
				iIndex = iIndex_descending;
			}
		}
	}
	if (ref_frame != NULL)
	{
		//delete ref_frame;
		ref_frame = NULL;

	}
	return iIndex;
}

__int64 TrainPackLoader::convertNumFrameToRelTime(int streamID_, unsigned __int64 nFrame)
{
	Frame* frame = NULL;
	frame = this->getVideoFrameByFrameNumberFromStream(streamID_, nFrame, true);

	int ret_timestamp = frame->timestampRelative;

	delete frame;
	return ret_timestamp;
}

hscamimport::HSEvent* TrainPackLoader::event(int eventNumber_)
{
	return nullptr;
}

void TrainPackLoader::printINFO()
{
}

int64_t TrainPackLoader::getAbsoluteTimestampFromString(std::string frameName)
{
	size_t lastPointPos = frameName.find_last_of(".");
	size_t firstUnderscorePos = frameName.find_last_of("_");
	std::string lastTimestamp = frameName.substr(firstUnderscorePos+1, lastPointPos-1);

	return stoll(lastTimestamp);
}

int64_t TrainPackLoader::getRelativeTimestamp(std::string frameName, int streamID_)
{
	int64_t absoluteTimestamp = this->getAbsoluteTimestampFromString(frameName);
	return (absoluteTimestamp - this->getAbsoluteStartTimeOfStream(streamID_));
}

void  TrainPackLoader::writeXML(boost::filesystem::path& filePath, ImageLoader* ImagesLoader, std::map<int, std::string>& streamsIndexes)
{
	boost::property_tree::ptree tree;
	std::string xmlFilePath;

	xmlFilePath = filePath.string() + ".xml";

	if (boost::filesystem::exists(xmlFilePath)) {
		try {
			boost::filesystem::remove(xmlFilePath);
		}
		catch (const boost::exception & e) {
			//if (e.what() == "cannot open file") cout << "sd";
			std::cout << diagnostic_information(e);

		}
	} //else : file will be created

	for (int streamIndex = 0; streamIndex < ImagesLoader->getNumStreams(); streamIndex++) {

		boost::property_tree::ptree StreamTree;

		StreamTree.add("AbsoluteStartTime", ImagesLoader->getAbsoluteStartTimeOfStream(streamIndex));
		StreamTree.add("RelStartTime", ImagesLoader->getRelStartTimeOfStream(streamIndex));
		StreamTree.add("Duration", ImagesLoader->getDurationOfStream(streamIndex));

		std::string streamName = streamsIndexes[streamIndex];


		//boost::property_tree::ptree ImagesTree;

		for (int frameIndex = 0; frameIndex < ImagesLoader->getNumFramesInStream(streamIndex); frameIndex++) {
			std::string FrameTagName = "frame_" + std::to_string(frameIndex);
			StreamTree.add(FrameTagName, std::to_string(frameIndex));

			Frame* frame = NULL;
			frame = ImagesLoader->getVideoFrameByFrameNumberFromStream(streamIndex, frameIndex, true);
			//cv::Mat twt = frame->getMat();
			StreamTree.put(FrameTagName + ".<xmlattr>.timestampRelative", frame->timestampRelative);
			StreamTree.put(FrameTagName + ".<xmlattr>.timestampAbsolute", frame->timestampAbsolute);
			delete frame;
		}

		tree.add_child("train." + streamName, StreamTree);
		//StreamTree.add_child("brakeStreamImages", ImagesTree);
	}

	//Add line breaks
	boost::property_tree::xml_writer_settings<std::string> settings(' ', 4);
	boost::property_tree::write_xml(xmlFilePath, tree, std::locale(), settings);
}
//#include "pch.h"
#include "distorsion_correction.h"

cv::Mat warpTriangle(cv::Mat & img1, std::vector<cv::Point2i> tri1, std::vector<cv::Point2i> tri2)
{
	// Find bounding rectangle for each triangle
	cv::Rect r1 = cv::boundingRect(tri1);
	r1 = cv::Rect(r1.x, r1.y, r1.width - 2, r1.height - 1);
	cv::Rect r2 = cv::boundingRect(tri2);
	r2 = cv::Rect(r2.x, r2.y, r2.width - 2, r2.height - 1);

	// Offset points by left top corner of the respective rectangles
	std::vector<cv::Point2f> tri1Cropped, tri2Cropped;
	std::vector<cv::Point> tri2CroppedInt;
	for (int i = 0; i < 3; i++)
	{
		tri1Cropped.push_back(cv::Point2f(tri1[i].x - r1.x, tri1[i].y - r1.y));
		tri2Cropped.push_back(cv::Point2f(tri2[i].x - r2.x, tri2[i].y - r2.y));

		// fillConvexPoly needs a vector of Point and not Point2f
		tri2CroppedInt.push_back(cv::Point((int)(tri2[i].x - r2.x), (int)(tri2[i].y - r2.y)));
	}

	// Apply warpImage to small rectangular patches
	cv::Mat img1Cropped;

	img1(r1).copyTo(img1Cropped);

	// Given a pair of triangles, find the affine transform.
	cv::Mat warpMat = cv::getAffineTransform(tri1Cropped, tri2Cropped);

	// Apply the Affine Transform just found to the src image
	cv::Mat img2Cropped = cv::Mat::zeros(r2.height, r2.width, CV_32FC1);//
	warpAffine(img1Cropped, img2Cropped, warpMat, img2Cropped.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT);

	// Get mask by filling triangle
	cv::Mat mask = cv::Mat::zeros(r2.height, r2.width, CV_8UC1);
	cv::fillConvexPoly(mask, tri2CroppedInt, cv::Scalar(1.0, 1.0, 1.0), 16, 0);
	cv::Mat result = img1.clone();

	img2Cropped.copyTo(result(r2), mask);

	return result;
}

void warpTriangles(cv::Mat& imgIn, cv::Mat& imgOut, std::vector<cv::Point2i> rectIn, std::vector<cv::Point2i> rectOut)
{
	//cv::Mat result;

	std::vector <cv::Point2i> triIn(rectIn.size());
	std::vector <cv::Point2i> triOut(rectOut.size());
	// copy matrix from warpTriangles_parameters_<name_of_station>.txt
	triIn[0] = rectIn[0];
	triIn[1] = rectIn[1];
	triIn[2] = rectIn[2];
	// copy matrix from warpTriangles_parameters_<name_of_station>.txt
	triOut[0] = rectOut[0];
	triOut[1] = rectOut[1];
	triOut[2] = rectOut[2];

	imgOut = warpTriangle(imgIn, triIn, triOut);

	//triIn.clear();
	//triOut.clear();
}

cv::Mat distorsion_correction(cv::Mat in_img, cv::Size original_size, std::vector<cv::Point2i> roi_corners, std::vector<cv::Point2i> dst_corners, bool use_perspective_correction)
{
	cv::Size2i new_size = cv::Size2i(in_img.cols + 180 * 2, in_img.rows + 167 * 2);
	cv::Mat imgOut;
	if (!use_perspective_correction) {
		imgOut = cv::Mat::zeros(new_size, in_img.type());
		in_img.copyTo(imgOut(cv::Rect2i((new_size.width - in_img.cols) / 2, (new_size.height - in_img.rows) / 2, in_img.cols, in_img.rows)));
	}
	else {
		imgOut = in_img.clone();
	}

	int num_of_roi_points = 3;
	std::vector< cv::Point2i> roi_corners_part(num_of_roi_points);
	//std::vector< cv::Point2i> roi_corners(num_of_roi_points * 4 );
	std::vector<cv::Point2i> dst_corners_part(num_of_roi_points);
	//std::vector<cv::Point2i> dst_corners(num_of_roi_points * 4);

	for (int parts_counter = 0; parts_counter < 4; parts_counter++)//save the coordinates for warping for writing in file
	{
		for (int j = 0; j < 3; j++)
		{
			roi_corners_part[j] = roi_corners[parts_counter*num_of_roi_points + j];
			dst_corners_part[j] = dst_corners[parts_counter*num_of_roi_points + j];
		}
		warpTriangles(imgOut, imgOut, roi_corners_part, dst_corners_part);
	}

	roi_corners_part.clear();
	//roi_corners.clear();
	dst_corners_part.clear();
	//dst_corners.clear();

	//return imgOut(cv::Rect(0, (imgOut.rows - original_size.height) / 2, imgOut.cols, original_size.height));//180 * 2
	return imgOut(cv::Rect(cv::Point2i(180, 167), cv::Point2i(imgOut.cols - 180, imgOut.rows - 167))); //cv::Size2i new_size = cv::Size2i(in_img.cols + 180 * 2, in_img.rows + 167 * 2);
}

#pragma once
#include "opencv2/imgproc.hpp"
#include "opencv2/calib3d.hpp"

cv::Mat perspective_correction(cv::Mat in_img, std::vector<cv::Point2i> roi_corners, std::vector<cv::Point2i> dst_corners);
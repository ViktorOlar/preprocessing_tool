# preprocessing_tool

FlexisBase.dll
HSCamImport.dll
Qt5Core.dll
Qt5Gui.dll
ucrtbase.dll
RaileyeIO.dll #RailEye
opencv_world420.dll
boost_filesystem-vc142-mt-x64-1_72.dll


-ENV=0 - HSCAM mode;-ENV=1 - RailEye mode;-ENV=2 - TrainPack mode 
Stream configuration: -<stream name>=first_frame-last_frame. Example: -BB=0-10 mean: process brakeblock-stream, frames 0 to 10; -UIC mean: process UIC-stream, all frames. (Values: UIC, BB, UICOP)

CMD example:
preprocessing_tool.exe -INPUT_DIR=C:\streams -OUTPUT_DIR=C:\output_dir\ -UIC -BB=0-10 -CONFIG=C:\configs\config.yaml -FORMAT=webp -QUALITY=75 -MAX_THREADS_NUMBER=3 -ENV=1 -USE_ALTALUX -USE_DISTCOR -USE_MSNAMES -USE_LIBWEBP -TRAINID=2020-04-17--08-58-36

-USE_LIBWEBP for using faster webp compression
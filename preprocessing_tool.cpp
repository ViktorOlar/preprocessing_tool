
#include <iostream>
#include <string>
#include <cctype>
#include <thread> 
#include <tbb/concurrent_queue.h>
#include <tbb/pipeline.h>
#include "../examples/common/utility/utility.h"
#include "../examples/common/utility/get_default_num_threads.h"
#include "imgs_lux_filter/CBaseAltaLuxFilter.h"
#include "imgs_lux_filter/CAltaLuxFilterFactory.h"
#include "perspective_correction.h"
#include "distorsion_correction.h"

#include "logging/LoggingSQL.h"
#include "logging/Logging.h"
#include <typeinfo>

#include "imageLoader/imageLoader.h"

std::wstring version = TARGET_NAME;
#include "libwebp/libwebp_iofunctions.h"//libwebp::imwrite

#define ALTALUX 0
#define DIST_COR 1

struct AltaLuxFParams
{
	int regions_size;
	int strength;
}AltaLuxParams;

struct DistorsionCorrParams
{
	cv::Size2i original_frame_size;
	bool use_distorsion_correction;
	std::vector<cv::Point2i> perspective_roi_points;
	std::vector<cv::Point2i> perspective_dst_points;
	std::vector<cv::Point2i> distorsion_roi_points;
	std::vector<cv::Point2i> distorsion_dst_points;
	bool perspective_corr_flag;
}DistorsionCorrectionParams;

size_t tbb_max_number_of_live_tokens;

struct stream
{
	int streamUicIndex;
	int streamBBIndex;
	int streamOppositeUicIndex;
}cfg_streamIndexes;

struct SQLLogging
{
	int logging_enabled;
	std::string logging_connectionString;
	std::string stationID;
	std::string machineID;
	std::string trainID;
}SQLLoggingParams;

volatile bool processingDone = false;
struct ProcessingChainData
{
	Frame* img;
	cv::Mat correctedPerspective, correctedDist;
	cv::Mat AltaLuxFiltered;
	cv::Mat imgToSave;
	int frameNumber;
};



template <typename T>
cv::Mat createMat_from_buffer(T* data, int rows, int cols, int chs = 1) {//create Mat from T* buffer
	// Create Mat from buffer 
	cv::Mat mat(rows, cols, CV_MAKETYPE(cv::DataType<T>::type, chs));
	memcpy(mat.data, data, rows*cols*chs * sizeof(T));
	return mat;
}
cv::FileStorage config_file;

void read_config_params(int read_mode);
void do_processing(ImageLoader* imgsloader, int iStream, cv::CommandLineParser& parser, cv::String output_folder, int firstFrameNr, int lastFrameNr, std::map<int, std::string>& streamsIndexes);
std::vector<int> getNumbersFromStr(cv::String in_str);
void InitSQLLogging();
void saveImage(ProcessingChainData* dataToSave, std::string outFolder, cv::CommandLineParser& parser);
void applyAtaLux(cv::Mat& image);

LoggingSQL SQLLog;
libwebp libwebpobj;
void removeSubstrs(std::wstring& originalStr, std::wstring& strToRemove) {
	std::string::size_type n = strToRemove.length();
	for (std::string::size_type i = originalStr.find(strToRemove);
		i != std::string::npos;
		i = originalStr.find(strToRemove))
		originalStr.erase(i, n);
}

int main(int argc, char* argv[])
{
//std::cout << cv::getBuildInformation() << std::endl;
	std::wstring  prefix = L"preprocessing_tool_";
	removeSubstrs(version, prefix);
	if (version.length() > 9)
	{
		ERROR << "Length of version extends maximum length: " << version;
		SQLLog.SetPollingOFF();
		std::exit(EXIT_FAILURE);
	}

	cv::String folders[2]; // 0: in; 1: out

	bool bHelp = false; // print help to console

	std::vector <int> writeFromStream;
	int maxThreadsNumber = 10;
	const std::string keys =
		"{ INPUT_DIR    |       |path to folder with recording. (MANDATORY)                                       }"
		"{ OUTPUT_DIR    |       |path to output folder. (MANDATORY)                                       }"
		"{ MAX_THREADS_NUMBER    |       |max number of threads. (MANDATORY)                                       }"
		"{ USE_ALTALUX    |       |use altalux filter. (optional)                                       }"
		"{ USE_DISTCOR    |       |use distorsion correction. (optional)                                       }"
		"{ CONFIG    |       |path to config file. (MANDATORY if correction or AltaLux is used)                                       }"
		"{ START_FRAME    |       |first frame number for export. (optional)                                       }"
		"{ END_FRAME    |       |last frame number for export. (optional)                                       }"
		"{ FORMAT    |       |format of output imgs. (optional)                                       }"
		"{ QUALITY    |       |0-9 for png, 01-100% for (m)jpg und webp. (optional)                                       }"
		"{ ENV    |       |0-2 0=HSCAM 1=RAILEYE 2=TRAINPACK. (optional)                                       }"
		"{ UIC    |       |Export UIC-stream. (MANDATORY)                                       }"
		"{ BB    |       |Export BB-stream. (MANDATORY)                                       }"
		"{ USE_MSNAMES    |       |Output images name format: if true: 00001.bmp; else: 1_<ms>.bmp. (optional)                                       }"
		"{ WRITEXML    |       |Write XML file to use TrainPack-mode. (optional)                                       }"
		"{ UICOP    |       |Export UIC-opposite-stream. (MANDATORY)                                       }"
		"{ USE_LIBWEBP    |       |Use libwebp for imwrite function. (optional)                                       }"
		"{ UICOP    |       |Export UIC-opposite-stream. (MANDATORY)                                       }"
		"{ TRAINID    |       |Train ID for SQL Logging. (MANDATORY)                                       }";
	cv::CommandLineParser parser(argc, argv, keys);
	//Log.AddLogEntry(LOG_INFO, "pPRE.REY", "READ", "Parse cmd parameters");
	//Parse MANDATORY arguments
	INFO << "Parse MANDATORY arguments";
	if (argc < 5)
	{
		ERROR << "Too few input parameters have been entered! (argc < 5)";
		SQLLog.SetPollingOFF();
		std::exit(EXIT_FAILURE);
	}
	else
	{
		if (parser.has("INPUT_DIR"))
		{

			folders[0] = parser.get<cv::String>("INPUT_DIR");
		}
		else {
			ERROR << "INPUT_DIR argument is required!";
			SQLLog.SetPollingOFF();
			std::exit(EXIT_FAILURE);
		}

		if (parser.has("OUTPUT_DIR"))
		{

			folders[1] = parser.get<cv::String>("OUTPUT_DIR");
		}
		else {
			ERROR << "OUTPUT_DIR argument is required!";
			SQLLog.SetPollingOFF();
			std::exit(EXIT_FAILURE);
		}

		if (parser.has("MAX_THREADS_NUMBER"))
		{

			maxThreadsNumber = parser.get<int>("MAX_THREADS_NUMBER");
		}
		else {
			ERROR << "MAX_THREADS_NUMBER argument is required!";
			SQLLog.SetPollingOFF();
			std::exit(EXIT_FAILURE);
		}

		if (parser.has("TRAINID"))
		{
			SQLLoggingParams.trainID = parser.get<std::string>("TRAINID");
		}
		else {
			ERROR << "TRAINID argument is required!";
			SQLLog.SetPollingOFF();
			std::exit(EXIT_FAILURE);
		}
	}

	boost::filesystem::path dir(folders[0]);
	//SQLLoggingParams.trainID = dir.filename().string();
	boost::replace_all(SQLLoggingParams.trainID, "--", "_");
	InitSQLLogging();
	
	INFO << "Opening config file and reading streams indexes and SQL Logging parameters";
	//Log.AddLogEntry(LOG_INFO, "pPRE.REY", "READ", "Reading config. parameters");
	cv::String config_file_name = parser.get<cv::String>("CONFIG");
	config_file.open(config_file_name, cv::FileStorage::READ);
	if (!config_file.isOpened())
	{
		//Log.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Failed to open config. file");
		ERROR << "Failed to open " + config_file_name;
		SQLLog.SetPollingOFF();
		std::exit(EXIT_FAILURE);
	}
	else
	{
		read_config_params(-1);
	}
	InitSQLLogging();

	SQLLog.AddLogEntry(LOG_INFO, "pPRE.REY", "START");

	int environment = 0;

	if (parser.has("ENV"))
	{
		environment = parser.get<int>("ENV");
	}

	try
	{
		std::vector<std::thread> threads;

		ImageLoader* ImagesLoader = NULL;
		std::string path = folders[0];

		switch (environment)
		{
		case HSCAM_ENV:
			ImagesLoader = ImageLoader::init(ImageLoader::ImageLoaderSource::HSCAM);
			INFO << "Try to load HSCAM recording";
			//path = "C:/Users/ViktorOlar-RailWatch/Desktop/ExportTool_data/streams/2019-10-28--20-45-06/2019-10-28--20-45-06";
			break;
		case RAILEYE_ENV:
			ImagesLoader = ImageLoader::init(ImageLoader::ImageLoaderSource::RAILEYE);
			INFO << "Try to load RAILEYE recording";
			//path = "C:\\Users\\ViktorOlar-RailWatch\\Desktop\\RailEye\\IOComponents_20200310\\TestIO\\Projects\\Project_20200226_165148";
			break;
		case TRAINPACK_ENV:
			ImagesLoader = ImageLoader::init(ImageLoader::ImageLoaderSource::TRAINPACK);
			path = "";
			break;
		default:
			break;
		}

		int errCode = 0;
		if (ImagesLoader != NULL)
		{
			std::map<int, std::string> streamsIndexes;
	
			streamsIndexes.insert(std::make_pair(cfg_streamIndexes.streamUicIndex, "UIC"));//
			streamsIndexes.insert(std::make_pair(cfg_streamIndexes.streamBBIndex, "BB"));
			streamsIndexes.insert(std::make_pair(cfg_streamIndexes.streamOppositeUicIndex, "UICOpposite"));

			ImagesLoader->load(path, &errCode, std::ref(streamsIndexes));
			ImagesLoader->printINFO();

			int streamsNr = ImagesLoader->getNumStreams();
			std::string sub_dir;
			int startFrameNum = 0;
			int lastFrameNum = 1;
			cv::String strNumbers = "";
			std::vector<int> intNumbersVec(2);
			std::map<int, std::vector<int>> framesNumbers;
			if (parser.has("BB"))
			{
				writeFromStream.push_back(cfg_streamIndexes.streamBBIndex);
				INFO << "Stream BB has: " << ImagesLoader->getNumFramesInStream(cfg_streamIndexes.streamBBIndex) << " frames";
				sub_dir = folders[1] + "/" + std::to_string(cfg_streamIndexes.streamBBIndex) + "_BB_" + std::to_string(ImagesLoader->getAbsoluteStartTimeOfStream(cfg_streamIndexes.streamBBIndex));
				if (!boost::filesystem::exists(sub_dir))
					boost::filesystem::create_directory(sub_dir);

				strNumbers = parser.get<cv::String>("BB");
				intNumbersVec = getNumbersFromStr(strNumbers);
				framesNumbers.insert(std::make_pair(cfg_streamIndexes.streamBBIndex, intNumbersVec));
			}

			if (parser.has("UIC"))
			{
				writeFromStream.push_back(cfg_streamIndexes.streamUicIndex);
				INFO << "Stream UIC has: " << ImagesLoader->getNumFramesInStream(cfg_streamIndexes.streamUicIndex) << " frames";
				sub_dir = folders[1] + "/" + std::to_string(cfg_streamIndexes.streamUicIndex) + "_UIC_" + std::to_string(ImagesLoader->getAbsoluteStartTimeOfStream(cfg_streamIndexes.streamUicIndex));
				if (!boost::filesystem::exists(sub_dir))
					boost::filesystem::create_directory(sub_dir);

				strNumbers = parser.get<cv::String>("UIC");
				intNumbersVec = getNumbersFromStr(strNumbers);
				framesNumbers.insert(std::make_pair(cfg_streamIndexes.streamUicIndex, intNumbersVec));
			}


			if (parser.has("UICOP"))
			{
				if (cfg_streamIndexes.streamOppositeUicIndex != -1) {

					writeFromStream.push_back(cfg_streamIndexes.streamOppositeUicIndex);
					INFO << "Stream UIC_Opposite has: " << ImagesLoader->getNumFramesInStream(cfg_streamIndexes.streamOppositeUicIndex) << " frames";
					sub_dir = folders[1] + "/" + std::to_string(cfg_streamIndexes.streamOppositeUicIndex) + "_UICOP_" + std::to_string(ImagesLoader->getAbsoluteStartTimeOfStream(cfg_streamIndexes.streamOppositeUicIndex));
					if (!boost::filesystem::exists(sub_dir))
						boost::filesystem::create_directory(sub_dir);

					strNumbers = parser.get<cv::String>("UICOP");
					intNumbersVec = getNumbersFromStr(strNumbers);
					framesNumbers.insert(std::make_pair(cfg_streamIndexes.streamOppositeUicIndex, intNumbersVec));
				}
				else
				{
					ERROR << "Recording does not contain a stream UIC-opposite or streamOppositeUicIndex==-1 in config. file!";
					SQLLog.SetPollingOFF();
					std::exit(EXIT_FAILURE);
				}
			}

			if (writeFromStream.empty()) {
				WARNING << "The list of streams is empty! Use parameters -UIC, -BB or -UICOP if you want to export images";
				//ERROR << "STREAM TYPE is required! (UIC, BB, UICOP)";
				SQLLog.SetPollingOFF();
				//std::exit(EXIT_FAILURE);
			}

			if (parser.has("USE_ALTALUX"))
			{
				SQLLog.AddLogEntry(LOG_INFO, "pPRE.REY", "READ", "Reading config. parameters for AltaLux");

				if (!config_file.isOpened())
				{
					ERROR << "Failed to open " + config_file_name + " reading parameters for ALTALUX";
					SQLLog.SetPollingOFF();
					std::exit(EXIT_FAILURE);
				}
				else
				{
					read_config_params( ALTALUX);
				}
			}

			if (parser.has("USE_DISTCOR"))
			{
				SQLLog.AddLogEntry(LOG_INFO, "pPRE.REY", "READ", "Reading config. parameters for DISTCOR");

				if (!config_file.isOpened())
				{
					ERROR << "Failed to open " + config_file_name + " reading parameters for DISTCOR";
					SQLLog.SetPollingOFF();
					std::exit(EXIT_FAILURE);
				}
				else
				{
					read_config_params(DIST_COR);
				}
			}

			//initWebPConfig
			if (parser.has("USE_LIBWEBP") && parser.get<cv::String>("FORMAT")=="webp")
			{
				INFO << "Using LIBWEBP";
				float quality = 75;
				if (parser.has("QUALITY"))
				{
					quality = parser.get<float>("QUALITY");
				}
				libwebpobj.initWebPConfig(config_file, quality);
				
			}

			if (parser.has("WRITEXML"))
			{
				boost::filesystem::path TrainPackXMLpath = folders[1] + "\\TrainPackXML";
				if (environment != TRAINPACK_ENV)
				{
					//INFO << "Start writing TrainPackXML...";
					SQLLog.AddLogEntry(LOG_INFO, "pPRE.REY", "WRITE", "Writing TrainPackXML...");
					//threads.push_back(std::thread(&TrainPackLoader::writeXML, TrainPackXMLpath, ImagesLoader, std::ref(streamsIndexes)));
					TrainPackLoader::writeXML(TrainPackXMLpath, ImagesLoader, std::ref(streamsIndexes));
					//INFO << "...done";
				}
			}

			int64 t0 = cv::getTickCount();
			INFO << "Recording has " << streamsNr << " streams";
			for (auto iStream : writeFromStream)
			{
				//INFO << "Read stream: " << iStream;
				SQLLog.AddLogEntry(LOG_INFO, "pPRE.REY", "READ", "Reading stream " + std::to_string(iStream));

				int streamLen = ImagesLoader->getNumFramesInStream(iStream);
				if (framesNumbers[static_cast<int>(iStream)][1] >= streamLen)
				{
					//ERROR << "Number of last frame is out of range. Stream length:" << streamLen;
					SQLLog.AddLogEntry(LOG_CRITICAL, "pPRE.REY", "ERROR", "Number of last frame is out of range.");
				}
				else if (framesNumbers[static_cast<int>(iStream)][1] != -1) {
					lastFrameNum = framesNumbers[static_cast<int>(iStream)][1];
				}
				else if (framesNumbers[static_cast<int>(iStream)][1] == -1)
				{
					lastFrameNum = streamLen - 1;
				}

				if (framesNumbers[static_cast<int>(iStream)][0] > lastFrameNum) {
					//ERROR << "Number of first frame > number of last frame.";
					SQLLog.AddLogEntry(LOG_CRITICAL, "pPRE.REY", "ERROR", "Number of first frame is  > number of last frame.");
				}
				else if (framesNumbers[static_cast<int>(iStream)][0] >= 0)
				{
					startFrameNum = framesNumbers[static_cast<int>(iStream)][0];
				}
				else if(framesNumbers[static_cast<int>(iStream)][1] == -1)
				{
					startFrameNum = 0;
				}

				INFO << "Creating a task pool";
				SQLLog.AddLogEntry(LOG_INFO, "pPRE.REY", "START", "Start processing stream " + std::to_string(iStream));
				if (streamLen > 0)
				{
					std::string subdir;
					subdir = streamsIndexes[iStream] + "_" + std::to_string(ImagesLoader->getAbsoluteStartTimeOfStream(iStream));
					cv::String outputImgFormat;
					cv::String outputFolder = folders[1] + "/" + std::to_string(iStream) + "_" + subdir;
					do_processing(ImagesLoader, iStream, std::ref(parser), folders[1], startFrameNum, lastFrameNum, std::ref(streamsIndexes));
				}
				else
				{
					SQLLog.AddLogEntry(LOG_CRITICAL, "pPRE.REY", "ERROR", "lenght of stream is 0");
				}
			} //for (auto iStream ..

			delete ImagesLoader;

			int64 t1 = cv::getTickCount();
			double secs = (t1 - t0) / cv::getTickFrequency();
			std::cout << "Processing duration: " << secs << " sec" << std::endl;
		}
		else
		{
			throw std::exception("ImageLoader is not initialized!");
		}
	}
	catch (const std::exception& e)
	{
		//INFO << typeid(RECTYPE).name() << "::Error: " << e.what();
		INFO << "ENV =" << environment << " ::Error: " << e.what();
	}
	//INFO << "preprocessing.exe completed successfully";
	SQLLog.AddLogEntry(LOG_INFO, "pPRE.REY", "DONE", "");
	SQLLog.SetPollingOFF();
	return 0;
} //main


std::vector<int> getNumbersFromStr(cv::String in_str) {
	std::vector<int> retVec(2);
	size_t hyphen_pos = in_str.find_first_of("-");

	cv::String temp;
	if (hyphen_pos != std::string::npos)
	{

		temp = in_str.substr(0, hyphen_pos);
		retVec[0] = std::stoi(temp);

		temp = in_str.substr(hyphen_pos + 1, in_str.length());
		retVec[1] = std::stoi(temp);
	}
	else
	{
		retVec[0] = -1;
		retVec[1] = -1;
	}

	return retVec;
}

void read_config_params(int read_mode)
{
	if (read_mode == ALTALUX)
	{
		//AltaLuxParams
		if (config_file["regions_size"].empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading contrast_coeff from config file");
			exit(EXIT_FAILURE);
		}
		AltaLuxParams.regions_size = static_cast<int>(config_file["regions_size"]);

		if (config_file["strength"].empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading contrast_coeff from config file");
			exit(EXIT_FAILURE);
		}
		AltaLuxParams.strength = static_cast<int>(config_file["strength"]);
	}

	if (read_mode == DIST_COR)
	{
		if (config_file["use_distortion_correction"].empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading distorsion_correction::use_distortion_correction from config file");
			exit(EXIT_FAILURE);
		}
		DistorsionCorrectionParams.perspective_corr_flag = static_cast<int>(config_file["use_distortion_correction"]) != 0;

		std::vector<int> p_roi_points;
		cv::FileNode node = config_file["perspective_correction"];
		if (node.empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading perspective_correction::roi_points from config file");
			exit(EXIT_FAILURE);
		}
		node["roi_corners"] >> p_roi_points;

		for (int i = 0; i < p_roi_points.size() - 1; i = i + 2)
		{
			DistorsionCorrectionParams.perspective_roi_points.push_back(cv::Point2i(p_roi_points[i], p_roi_points[i + 1]));
		}

		std::vector<int> p_dst_points;
		node = config_file["perspective_correction"];
		if (node.empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading perspective_correction::dst_points from config file");
			exit(EXIT_FAILURE);
		}
		node["dst_corners"] >> p_dst_points;

		for (int i = 0; i < p_dst_points.size() - 1; i = i + 2)
		{
			DistorsionCorrectionParams.perspective_dst_points.push_back(cv::Point2i(p_dst_points[i], p_dst_points[i + 1]));
		}

		std::vector<int> d_roi_points;
		node = config_file["distorsion_correction"];
		if (node.empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading distorsion_correction::roi_points from config file");

			exit(EXIT_FAILURE);
		}
		node["roi_corners"] >> d_roi_points;

		for (int i = 0; i < d_roi_points.size() - 1; i = i + 2)
		{
			DistorsionCorrectionParams.distorsion_roi_points.push_back(cv::Point2i(d_roi_points[i], d_roi_points[i + 1]));
		}

		std::vector<int> d_dst_points;
		node = config_file["distorsion_correction"];
		if (node.empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading distorsion_correction::dst_points from config file");

			exit(EXIT_FAILURE);
		}
		node["dst_corners"] >> d_dst_points;

		for (int i = 0; i < d_dst_points.size() - 1; i = i + 2)
		{
			DistorsionCorrectionParams.distorsion_dst_points.push_back(cv::Point2i(d_dst_points[i], d_dst_points[i + 1]));
		}	
	}

	if (read_mode == -1)
	{
		if (config_file["streamBBIndex"].empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading streamBBIndex from config file");
			exit(EXIT_FAILURE);
		}
		cfg_streamIndexes.streamBBIndex = static_cast<int>(config_file["streamBBIndex"]);
		
		if (config_file["streamOppositeUicIndex"].empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading streamOppositeUicIndex from config file");
			exit(EXIT_FAILURE);
		}
		cfg_streamIndexes.streamOppositeUicIndex = static_cast<int>(config_file["streamOppositeUicIndex"]);
		
		if (config_file["streamUicIndex"].empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading streamUicIndex from config file");
			exit(EXIT_FAILURE);
		}
		cfg_streamIndexes.streamUicIndex = static_cast<int>(config_file["streamUicIndex"]);

		if (config_file["logging_enabled"].empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading logging_enabled from config file");
			exit(EXIT_FAILURE);
		}
		SQLLoggingParams.logging_enabled = static_cast<int>(config_file["logging_enabled"]);

		if (config_file["logging_connectionString"].empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading logging_connectionString from config file");
			exit(EXIT_FAILURE);
		}
		SQLLoggingParams.logging_connectionString = static_cast<std::string>(config_file["logging_connectionString"]);

		if (config_file["stationID"].empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading stationID from config file");
			exit(EXIT_FAILURE);
		}
		SQLLoggingParams.stationID = static_cast<std::string>(config_file["stationID"]);

		if (config_file["machineID"].empty())
		{
			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading machineID from config file");
			exit(EXIT_FAILURE);
		}
		SQLLoggingParams.machineID = static_cast<std::string>(config_file["machineID"]);
	}
	if (config_file["max_number_of_live_tokens"].empty())
	{
		SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Error when reading max_number_of_live_tokens from config file");
		exit(EXIT_FAILURE);
	}
	tbb_max_number_of_live_tokens = static_cast<int>(config_file["max_number_of_live_tokens"]);
}

void do_processing(ImageLoader* imagesLoader, int iStream, cv::CommandLineParser& parser, cv::String output_folder, int firstFrameNr, int lastFrameNr, std::map<int, std::string>& streamsIndexesStr)
{	
	int frame_number = firstFrameNr;
	std::string subdir;
	subdir = streamsIndexesStr[iStream] + "_" + std::to_string(imagesLoader->getAbsoluteStartTimeOfStream(iStream));
	cv::String outputFolder = output_folder + "/" + std::to_string(iStream) + "_" + subdir;

	tbb::parallel_pipeline(tbb_max_number_of_live_tokens,

		tbb::make_filter<void, ProcessingChainData*>(
			tbb::filter::serial,
			[&](tbb::flow_control& fc)-> ProcessingChainData* {
				ProcessingChainData* pData = new ProcessingChainData;

				if (frame_number > lastFrameNr)
				{
					INFO << "LAST_Frame number: " << frame_number;
					delete pData;
					processingDone = true;
					fc.stop();
				}
				else {
					pData->img = imagesLoader->getVideoFrameByFrameNumberFromStream(iStream, frame_number, true);
					pData->frameNumber = frame_number;
					frame_number++;
				}

				return pData; }
			) &
		tbb::make_filter<ProcessingChainData*, ProcessingChainData*>(
					tbb::filter::parallel,
					[&](ProcessingChainData* pData)-> ProcessingChainData* {

						pData->AltaLuxFiltered = pData->img->getMat();
						if (parser.has("USE_ALTALUX"))
						{
							INFO << "Frame number: " << frame_number << " ::AltaLux filter processing";
							applyAtaLux(pData->AltaLuxFiltered);
						}

						return pData;
					}
					)&
					tbb::make_filter<ProcessingChainData*, ProcessingChainData*>(
											tbb::filter::parallel,
											[&](ProcessingChainData* pData)-> ProcessingChainData* {

												pData->correctedPerspective = pData->AltaLuxFiltered;
												if (parser.has("USE_DISTCOR") && streamsIndexesStr[iStream] == "UIC") {
													try {
														INFO << "Frame number: " << frame_number << " ::perspective correction";
														pData->correctedPerspective = perspective_correction(pData->AltaLuxFiltered, DistorsionCorrectionParams.perspective_roi_points, DistorsionCorrectionParams.perspective_dst_points);
													}
													catch (cv::Exception& e) {
														SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Frame " + std::to_string(frame_number) + " ::persp_corr exception ");
														ERROR << e.what();
													}
												}

												return pData;
											}
											)&
															tbb::make_filter<ProcessingChainData*, ProcessingChainData*>(
																tbb::filter::parallel,
																[&](ProcessingChainData* pData)-> ProcessingChainData* {

																	pData->correctedDist = pData->AltaLuxFiltered;
																	if (parser.has("USE_DISTCOR") && streamsIndexesStr[iStream] == "UIC") {
																		try {
																			INFO << "Frame number: " << frame_number << " ::distorsion correction";
																			DistorsionCorrectionParams.original_frame_size = cv::Size(pData->correctedPerspective.cols, pData->correctedPerspective.rows);
																			pData->correctedDist = distorsion_correction(pData->correctedPerspective, DistorsionCorrectionParams.original_frame_size, DistorsionCorrectionParams.distorsion_roi_points, DistorsionCorrectionParams.distorsion_dst_points, DistorsionCorrectionParams.perspective_corr_flag);
																		}
																		catch (cv::Exception& e) {
																			SQLLog.AddLogEntry(LOG_ERROR, "pPRE.REY", "ERROR", "Frame " + std::to_string(frame_number) + " ::dist_corr exception ");
																			ERROR << e.what();
																		}
																	}

																	return pData;
																})&
					tbb::make_filter<ProcessingChainData*, void>(
						tbb::filter::parallel,
						[&](ProcessingChainData* pData) {

							pData->imgToSave = pData->correctedPerspective;

							try
							{
								
								INFO << "Frame number: " << pData->frameNumber << " ::Frame writing";
								//INFO << corrected_frame.type();
								if (pData->imgToSave.empty())
								{
									//ERROR << "Result image is empty!";
									SQLLog.AddLogEntry(LOG_CRITICAL, "pPRE.REY", "ERROR", std::to_string(pData->frameNumber) + " ::Result image is empty! ");
								}
								else
								{
									saveImage(pData, outputFolder, parser);
									delete pData->img;
									delete pData;
								}
							}
							catch (const cv::Exception& e)
							{
								ERROR << e.what();
								delete pData->img;
								delete pData;
							}

						}
						)
		
		);
	
}


void InitSQLLogging()
{
	INFO<< "Initialize SQL-Logging ...";
	boost::replace_all(SQLLoggingParams.logging_connectionString, "{{SQL Server}}", "{SQL Server}");

	SQLLog.SetSilentON();
	SQLLog.SetConnectionString(SQLLoggingParams.logging_connectionString);
	SQLLog.SetStationID(SQLLoggingParams.stationID);
	SQLLog.SetMachineID(SQLLoggingParams.machineID);
	SQLLog.SetTimeout(200);
	SQLLog.SetVersion(std::string(version.begin(), version.end()));
	SQLLog.SetTrainID(SQLLoggingParams.trainID);
	SQLLog.SetRetryOFF();

	if (SQLLoggingParams.logging_enabled == 1) {
		SQLLog.SetLoggingON();
		INFO << "Initialize SQL-Logging done and switched on!";
	}
	else {
		INFO << "Initialize SQL-Logging done and switched off!";
	}
}

void saveImage(ProcessingChainData* dataToSave, std::string outFolder, cv::CommandLineParser& parser) {
	std::vector<int> write_params_vec;
	std::string frameNumberStr = std::to_string(dataToSave->frameNumber);
	cv::Mat matToSave;
	//std::string name_to_write;
	cv::String outputImgFormat;

	std::string filename;

	dataToSave->imgToSave.copyTo(matToSave);

	if (parser.has("FORMAT"))
	{
		outputImgFormat = parser.get<cv::String>("FORMAT");
	}
	else {
		outputImgFormat = "bmp";
	}

	if (parser.has("USE_MSNAMES")/*&& parser.get<int>("ENV")!= TRAINPACK_ENV*/)
	{
		if (dataToSave->img->name.empty())
		{
			frameNumberStr = std::to_string(dataToSave->frameNumber) + "_" + std::to_string((int)(dataToSave->img->timestampRelative / 1000000));
		}
		else {
			frameNumberStr = dataToSave->img->name;
		}
	}
	else {
		if (frameNumberStr.length() < 5)
		{
			int nullNumber = 5 - frameNumberStr.length();
			for (int i = 0; i < nullNumber; i++)
			{
				frameNumberStr = "0" + frameNumberStr;
			}
		}
	}

	if (parser.has("USE_MSNAMES"))
	{
		filename = outFolder + "/" + frameNumberStr + "." + outputImgFormat;
	}
	else {
		filename = outFolder + "/" + std::to_string(dataToSave->frameNumber) + "_" + std::to_string(dataToSave->img->timestampAbsolute) + "." + outputImgFormat;
	}

		if (parser.get<std::string>("FORMAT") == "bmp")
		{
			//outputImgFormat == ".bmp";
			cv::imwrite(filename, matToSave);
		}
		else if (parser.get<std::string>("FORMAT") == "png")
		{
			//outputImgFormat == ".png";
			write_params_vec.push_back(cv::IMWRITE_PNG_COMPRESSION);//
			if (parser.has("QUALITY"))
			{
				write_params_vec.push_back(parser.get<int>("QUALITY"));
			}
			else
			{
				write_params_vec.push_back(9);
			}
			cv::imwrite(filename, matToSave, write_params_vec);
		}
		else if (parser.get<std::string>("FORMAT") == "webp")
		{
			write_params_vec.push_back(cv::IMWRITE_WEBP_QUALITY);//IMWRITE_WEBP_QUALITY
			if (parser.has("QUALITY"))
			{
				write_params_vec.push_back(parser.get<int>("QUALITY"));
			}
			else
			{
				write_params_vec.push_back(75);
			}

			if (parser.has("USE_LIBWEBP"))
			{
				if (libwebpobj.configInitialized)
				{
					if (matToSave.channels() < 3)
						cv::cvtColor(matToSave, matToSave, cv::COLOR_GRAY2BGR);
					libwebpobj.imwrite(filename, matToSave);
				}
				else {
					ERROR << "libwebp: config. parameters was not initialized!";
				}
			}
			else {
				cv::imwrite(filename, matToSave, write_params_vec);
			}


		}
		else if (parser.get<std::string>("FORMAT") == "jpg")
		{
			//outputImgFormat == ".jpg";
			write_params_vec.push_back(cv::IMWRITE_JPEG_QUALITY);//IMWRITE_WEBP_QUALITY
			if (parser.has("QUALITY"))
			{
				write_params_vec.push_back(parser.get<int>("QUALITY"));
			}
			else
			{
				write_params_vec.push_back(75);
			}
			cv::imwrite(filename, matToSave, write_params_vec);
		}
		else if (parser.get<std::string>("FORMAT") == "tiff")
		{
			cv::imwrite(filename, matToSave);
		}
	
}

void applyAtaLux(cv::Mat& image)
{
	if (image.channels() < 3)
		cv::cvtColor(image, image, cv::COLOR_GRAY2BGR);
	std::unique_ptr<CBaseAltaLuxFilter> AltaLuxFilter(CAltaLuxFilterFactory::CreateAltaLuxFilter(image.cols, image.rows, ALTALUX_FILTER_ACTIVE_WAIT, AltaLuxParams.regions_size, AltaLuxParams.regions_size));//ALTALUX_FILTER_ACTIVE_WAIT
	AltaLuxFilter->SetStrength(AltaLuxParams.strength);
	int64 t0 = cv::getTickCount();
	AltaLuxFilter->ProcessRGB24(image.data);
	//int64 t1 = cv::getTickCount();
	//double imwritesecs = (t1 - t0) / cv::getTickFrequency();
	//INFO << "Altalux duration: " << imwritesecs;
	cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
}

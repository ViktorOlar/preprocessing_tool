#pragma once
#include "opencv2/imgproc.hpp"
#include "opencv2/calib3d.hpp"

cv::Mat warpTriangle(cv::Mat &img1, std::vector<cv::Point2i> tri1, std::vector<cv::Point2i> tri2);
void warpTriangles(cv::Mat& imgIn, cv::Mat& imgOut, std::vector<cv::Point2i> rectIn, std::vector<cv::Point2i> rectOut);
cv::Mat distorsion_correction(cv::Mat in_img, cv::Size original_size, std::vector<cv::Point2i> distorsion_roi_points, std::vector<cv::Point2i> distorsion_dst_points, bool use_perspective_correction);
#define NOMINMAX
#include <windows.h>
#include <sql.h>
#include <sqlext.h>
#include <string>
#include <sstream>
#include <iostream>
#include <deque>
#include <thread>
#include <mutex>
#include <thread>
#include "Logging.h"

#ifndef LOGGINGSQL_H
#define LOGGINGSQL_H

#define LOG_CRITICAL 1
#define LOG_ERROR 2
#define LOG_WARNING 3
#define LOG_INFO 4
#define LOG_DEBUG 5

#define DB_TEST 0
#define DB_PROD 1

#define LOG_COMMAND_MAXSIZE 10
#define LOG_MESSAGE_MAXSIZE 256
#define LOG_PROCESSNAME_MAXSIZE 25
#define LOG_FULLSTATIONID_MAXSIZE 15
#define LOG_MACHINEID_MAXSIZE 25
#define LOG_VERSION_MAXSIZE 10

#define SQL_LOGGING_TABLE "Logging"
#define SQL_LOGGING_SCHEMA "SCHEMA_LOCAL_DB"
#define SQL_QUERY_SIZE 1000

class LoggingSQL
{
public:
	LoggingSQL();
	~LoggingSQL();

	void SetConnectionString(std::string ConnectionString) { mConnectionString = ConnectionString; };
	void SetSilentON() { mSilent = true; };
	void SetSilentOFF() { mSilent = false; };
	void SetRetryON() { mRetry = true; };
	void SetRetryOFF() { mRetry = false; };
	void SetStationID(std::string StationID);
	void SetTrainID(std::string TrainID);
	void SetMachineID(std::string MachineID);
	void SetLoggingON() { mAllowLogging = true; };
	void SetLoggingOFF() { mAllowLogging = false; };
	void SetTimeout(int timeoutMs) { mExecuteTimeout = timeoutMs; };
	void SetPollingOFF();
	void SetVersion(std::string Version);

	void QueryHandler();
	void QueryThread();

	int AddLogEntry(int LogLevel, std::string ProcessName, std::string Command, std::string Message = "");


private:
	std::string GetTrainIDAsTimestamp();

	bool Connect();
	void Disconnect();

	std::unique_ptr<std::deque<std::string>> mExecuteQueue;
	std::unique_ptr<std::thread> mExecuteThread;

	std::mutex mutex;

	std::string mConnectionString;
	std::string mStationID = "";
	std::string mTrainID; // can be empty
	std::string mMachineID = "";
	std::string mVersion = "";

	bool mSilent;
	bool mRetry;
	bool mFailedToConnect;
	bool mAllowLogging;
	bool mIsExecuting;
	bool mEndPolling;

	int mExecuteTimeout;

	SQLHENV     hEnv = NULL;
	SQLHDBC     hDbc = NULL;
	SQLHSTMT    hStmt = NULL;
	WCHAR*      pwszConnStr;
	WCHAR       wszInput[SQL_QUERY_SIZE];
	SQLRETURN RetCode;
	SQLSMALLINT sNumResults;
};

#endif

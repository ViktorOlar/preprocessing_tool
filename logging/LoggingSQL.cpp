#include "LoggingSQL.h"

using namespace std;

LoggingSQL::LoggingSQL()
{
	mExecuteQueue = std::make_unique<std::deque<std::string>>();
	mExecuteThread = std::make_unique<std::thread>(&LoggingSQL::QueryThread, this);
	mSilent = false;
	mRetry = false;
	mFailedToConnect = false;
	mAllowLogging = false;
	mIsExecuting = false;
	mExecuteTimeout = 100;
}

LoggingSQL::~LoggingSQL()
{

}

std::string LoggingSQL::GetTrainIDAsTimestamp()
{
	std::string TrainIDYYYYMMDD = mTrainID.substr(0, 10);
	std::string TrainIDHH = mTrainID.substr(11, 2);
	std::string TrainIDMM = mTrainID.substr(14, 2);
	std::string TrainIDSS = mTrainID.substr(17, 2);

	return TrainIDYYYYMMDD + " " + TrainIDHH + ":" + TrainIDMM + ":" + TrainIDSS;
}

void LoggingSQL::SetStationID(std::string StationID)
{
	if (StationID.size() > LOG_FULLSTATIONID_MAXSIZE && !mSilent) std::cout << "Warning: Station ID exceeds maximum number of allowed characters!" << std::endl;

	mStationID = StationID;
}

void LoggingSQL::SetTrainID(std::string TrainID)
{
	mTrainID = TrainID;
}

void LoggingSQL::SetMachineID(std::string MachineID)
{
	if (MachineID.size() > LOG_MACHINEID_MAXSIZE && !mSilent) std::cout << "Warning: Machine ID exceeds maximum number of allowed characters!" << std::endl;

	mMachineID = MachineID;
}

void LoggingSQL::SetVersion(std::string Version)
{
	if (Version.size() > LOG_VERSION_MAXSIZE && !mSilent) std::cout << "Warning: Version exceeds maximum number of allowed characters!" << std::endl;

	mVersion = Version;
}


bool LoggingSQL::Connect()
{
	int bConnected = false;

	// Allocate environment handle  
	RetCode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &hEnv);

	if (RetCode == SQL_SUCCESS || RetCode == SQL_SUCCESS_WITH_INFO)
	{
		// Set the ODBC version environment attribute  
		RetCode = SQLSetEnvAttr(hEnv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, 0);

		if (RetCode == SQL_SUCCESS || RetCode == SQL_SUCCESS_WITH_INFO)
		{
			// Allocate connection handle  
			RetCode = SQLAllocHandle(SQL_HANDLE_DBC, hEnv, &hDbc);

			if (RetCode == SQL_SUCCESS || RetCode == SQL_SUCCESS_WITH_INFO)
			{
				// Set login timeout to 5 seconds  
				SQLSetConnectAttr(hDbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

				// Connect to data source  
				RetCode = SQLDriverConnect(hDbc, NULL, (WCHAR*)wstring(mConnectionString.begin(), mConnectionString.end()).c_str(), SQL_NTS, NULL, 0, NULL, SQL_DRIVER_COMPLETE);

				if (RetCode == SQL_SUCCESS || RetCode == SQL_SUCCESS_WITH_INFO)
				{
					bConnected = true;
				}
			}
			else {
				SQLFreeHandle(SQL_HANDLE_DBC, hDbc);
			}
		}
	}
	else {
		SQLFreeHandle(SQL_HANDLE_ENV, hEnv);
	}

	return bConnected;
}

void LoggingSQL::Disconnect()
{
	SQLDisconnect(hDbc);
	SQLFreeHandle(SQL_HANDLE_DBC, hDbc);
	SQLFreeHandle(SQL_HANDLE_ENV, hEnv);
}

void LoggingSQL::SetPollingOFF()
//Call this seperately at the end of main function, because if it's called in destructor, the thread doesn't end appropriately due to a bug within VS
{ 	
	if (mExecuteThread->joinable()) {
		mEndPolling = true;
		mExecuteThread->join();
	}
	else {
		cout << "Logging-Thread not joinable." << std::endl;
	}
}

int LoggingSQL::AddLogEntry(int LogLevel, std::string ProcessName, std::string Command, std::string Message)
{
	//writing log message in std
	string std_log_message = ProcessName + "::" + Command ;
	if (!Message.empty())
	{
		std_log_message = std_log_message + "::" + Message;
	}

	switch (LogLevel)
	{
	case LOG_CRITICAL:
		CRITICAL << std_log_message;
		break;
	case LOG_ERROR:
		ERROR << std_log_message;
		break;
	case LOG_WARNING:
		WARNING << std_log_message;
		break;
	case LOG_INFO:
		INFO << std_log_message;
		break;
	case LOG_DEBUG:
		DEBUG << std_log_message;
		break;
	default:
		break;
	}

	if (Command.size() > LOG_COMMAND_MAXSIZE)
	{
		if (!mSilent) std::cout << "Warning: Command exceeds maximum number of allowed characters! String will be truncated..." << std::endl;
		return 2;
	}

	if (ProcessName.size() > LOG_PROCESSNAME_MAXSIZE)
	{
		if (!mSilent) std::cout << "Warning: Process Name exceeds maximum number of allowed characters! String will be truncated..." << std::endl;
		return 2;
	}

	if (Message.size() > LOG_MESSAGE_MAXSIZE)
	{
		if (!mSilent) std::cout << "Warning: Message exceeds maximum number of allowed characters! String will be truncated..." << std::endl;
		return 2;
	}

	if (mVersion == "")
	{
		if (!mSilent) std::cout << "Warning: Please define a version number! Entry ignored..." << std::endl;
		return 1;
	}

	//Convert to std:string
	stringstream ssSQLLogLevel;
	ssSQLLogLevel << LogLevel;
	std::string sSQLLogLevel = ssSQLLogLevel.str();

	//Get current process id as int
	DWORD dProcessID = GetCurrentProcessId();
	stringstream ssProcessID;
	ssProcessID << dProcessID;
	std::string sProcessID = ssProcessID.str();

	//Get current thread id as int
	DWORD dThreadID = GetCurrentThreadId();
	stringstream ssThreadID;
	ssThreadID << dThreadID;
	std::string sThreadID = ssThreadID.str();

	std::string SQLQuery = "INSERT INTO [";
	SQLQuery += SQL_LOGGING_SCHEMA;
	SQLQuery += "].[";
	SQLQuery += SQL_LOGGING_TABLE;
	SQLQuery += "] (date, log_level, approved, full_station_id, command, message, version, process_name, process_id, thread_id, train_id, machine_id) VALUES (SYSDATETIMEOFFSET(), ";
	SQLQuery += sSQLLogLevel;
	SQLQuery += ", 0, '";
	SQLQuery += mStationID;
	SQLQuery += "', '";
	SQLQuery += Command,
	SQLQuery += "', '";
	SQLQuery += Message;
	SQLQuery += "', '";
	SQLQuery += mVersion,
	SQLQuery += "', '";
	SQLQuery += ProcessName;
	SQLQuery += "', '";
	SQLQuery += sProcessID;
	SQLQuery += "', '";
	SQLQuery += sThreadID;
	SQLQuery += "', '";
	SQLQuery += GetTrainIDAsTimestamp();
	SQLQuery += "', '";
	SQLQuery += mMachineID;
	SQLQuery += "')";

	mutex.lock();
	mExecuteQueue->emplace_back(SQLQuery);
	mutex.unlock();

	return 0;
}

void LoggingSQL::QueryThread()
{
	while (!mEndPolling || mExecuteQueue->size() > 0)
	{
		if (mExecuteQueue->size() > 0)
		{
			QueryHandler();
		}

		Sleep(mExecuteTimeout);
	}
}

void LoggingSQL::QueryHandler()
{
	if ((mFailedToConnect || !mRetry) && !mAllowLogging)
	{
		mFailedToConnect = true;
		if (!mSilent) std::cout << "Error: Can not connect to SQL database!" << std::endl;
	}

	if (!Connect())
	{
		if (!mSilent) std::cout << "Error: Can not connect to SQL database!" << std::endl;
	}

	while (mExecuteQueue->size() > 0)
	{
		mutex.lock();
		std::string SQLQuery = mExecuteQueue->front();
		mutex.unlock();

		SQLAllocHandle(SQL_HANDLE_STMT, hDbc, &hStmt);
		RetCode = SQLExecDirect(hStmt, (WCHAR*)wstring(SQLQuery.begin(), SQLQuery.end()).c_str(), SQL_NTS);

		if (RetCode == SQL_SUCCESS || RetCode == SQL_SUCCESS_WITH_INFO)
		{
			if (!mSilent) std::cout << "Successfully executed SQL query." << std::endl;
		}
		else
		{
			if (!mSilent) std::cout << "Error: Something is wrong with the SQL query!" << std::endl;
			if (!mSilent) std::cout << "Query Log: " << SQLQuery << std::endl;
			if (!mSilent) std::cout << "Executing the remaining queries";
		}

		mutex.lock();
		mExecuteQueue->pop_front();
		mutex.unlock();
	}

	mIsExecuting = false;
	Disconnect();
}
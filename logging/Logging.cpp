
#include "Logging.h"
namespace attrs = boost::log::attributes;
namespace expr = boost::log::expressions;
namespace logging = boost::log;
namespace sinks = boost::log::sinks;


// The operator puts a human-friendly representation of the severity level to the stream
std::ostream& operator<< (std::ostream& strm, customSeverityLevels level)
{
	static const char* strings[] =
	{
		"CRITICAL",
		"ERROR",
		"WARNING",
		"INFO",
		"DEBUG",
	};

	if (static_cast<std::size_t>(level) < sizeof(strings) / sizeof(*strings))
		strm << strings[level];
	else
		strm << static_cast<int>(level);

	return strm;
}

//Defines a global logger initialization routine
BOOST_LOG_GLOBAL_LOGGER_INIT(my_logger, logger_t)
{
	logger_t lg;

	logging::add_common_attributes();	
	auto severity = expr::attr<customSeverityLevels>("Severity");

	logging::add_console_log(
		std::cout,
		boost::log::keywords::format = (
			expr::stream << expr::format_date_time<     boost::posix_time::ptime >("TimeStamp", "[%Y-%m-%d_%H-%M-%S]")<<
			" ["<< severity << "]        "
			<< expr::smessage), 
		boost::log::keywords::auto_flush = true
	);
	
	logging::core::get()->set_filter
	(
		boost::log::expressions::attr<customSeverityLevels>("Severity") >= customSeverityLevels::critical
	);

	return lg;
}
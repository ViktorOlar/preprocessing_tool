#pragma once

#include <boost/log/expressions.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared_object.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/core/null_deleter.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

enum customSeverityLevels
{
	critical,
	error,
	warning,
	info,
	debug,
};


#define INFO  BOOST_LOG_SEV(my_logger::get(), customSeverityLevels::info)
#define WARNING  BOOST_LOG_SEV(my_logger::get(), customSeverityLevels::warning)
#define ERROR BOOST_LOG_SEV(my_logger::get(), customSeverityLevels::error)
#define DEBUG BOOST_LOG_SEV(my_logger::get(), customSeverityLevels::debug) 
#define CRITICAL BOOST_LOG_SEV(my_logger::get(), customSeverityLevels::critical) 

//Narrow-char thread-safe logger.
typedef boost::log::sources::severity_logger_mt<customSeverityLevels> logger_t;

//declares a global logger with a custom initialization
BOOST_LOG_GLOBAL_LOGGER(my_logger, logger_t)
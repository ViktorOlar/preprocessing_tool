#pragma once
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>

#include "examples/example_util.h"
#include "imageio/image_dec.h"
#include "imageio/imageio_util.h"
#include "webp/encode.h"

#include "examples/stopwatch.h"
#include "examples/unicode.h"

#include <opencv2/core/mat.hpp>
#include <opencv2/core/persistence.hpp>

#include "../logging/Logging.h"

#ifndef WEBP_DLL
#ifdef __cplusplus
extern "C" {
#endif

    extern void* VP8GetCPUInfo;   // opaque forward declaration.

#ifdef __cplusplus
}    // extern "C"
#endif
#endif  // WEBP_DLL

class libwebp
{
public:
    libwebp();
    ~libwebp();

    cv::Mat imread(std::string filename);
    void imwrite(const std::string& filename, cv::Mat img);

    void initWebPConfig(cv::FileStorage config_file, float qualityValue);

    bool configInitialized;
    int enabled = 0;
private:
    int verbose = 0;
    
    int short_output = 1;
    int quiet = 1;
    int keep_alpha = 1;
    int blend_alpha = 0;
    int crop = 0, crop_x = 0, crop_y = 0, crop_w = 0, crop_h = 0;
    int resize_w = 0, resize_h = 0;
    int use_lossless_preset = -1;  // -1=unset, 0=don't use, 1=use it
    int lossless_preset = 6; //activates lossless preset with given level in [0:fast, ..., 9:slowest]
    int show_progress = 0;
    int keep_metadata = 0;
    int metadata_written = 0;
    int print_distortion = -1;        // -1=off, 0=PSNR, 1=SSIM, 2=LSIM
    WebPConfig config;

    uint32_t background_color = 0xffffffu;
    WebPPicture picture;
    WebPPicture original_picture;    // when PSNR or SSIM is requested
    WebPAuxStats stats;
    WebPMemoryWriter memory_writer;
    Metadata metadata;
    Stopwatch stop_watch;

    int ReadPicture(cv::Mat img, WebPPicture* const pic);
    int ReadPicture(std::string filename, WebPPicture* const pic,
        int keep_alpha, Metadata* const metadata);
};